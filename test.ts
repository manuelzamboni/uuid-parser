import { parse, unparse } from "./index";
import * as chai from "chai";
const expect = chai.expect;

describe("parse", () => {
    it("returns bytes from UUID string", () => {
        // Mixes uppercase, lowercase and zeroes
        const bytes = parse("4e1a590F-46d3-4100-9804-49e410f7dc83");
        expect(bytes).to.eql([
            0x0f,
            0x59,
            0x1a,
            0x4e,
            0xd3,
            0x46,
            0x00,
            0x41,
            0x98,
            0x04,
            0x49,
            0xe4,
            0x10,
            0xf7,
            0xdc,
            0x83
        ]);
    });
});

describe("unparse", () => {
    it("returns string from UUID's bytes", () => {
        const array = [
            0x0f,
            0x59,
            0x1a,
            0x4e,
            0xd3,
            0x46,
            0x00,
            0x41,
            0x98,
            0x04,
            0x49,
            0xe4,
            0x10,
            0xf7,
            0xdc,
            0x83
        ];
        const buffer = Buffer.from(array);

        expect(unparse(array)).to.equal("4e1a590f-46d3-4100-9804-49e410f7dc83");
        expect(unparse(buffer)).to.equal(
            "4e1a590f-46d3-4100-9804-49e410f7dc83"
        );
    });
});

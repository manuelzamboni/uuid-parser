"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Returns the bytes for the given UUID's string representation.
 */
function parse(str) {
    const bytes = [];
    const bytesRegex = /.{2}/g;
    str.split("-").map((section, index) => {
        // Bytes in the first 3 sections of the UUID are stored in reversed
        // direction with respect to the string representation
        const sectionDigits = index < 3
            ? section.match(bytesRegex).reverse()
            : section.match(bytesRegex);
        bytes.push(...sectionDigits.map(digits => parseInt(digits, 16)));
    });
    return bytes;
}
exports.parse = parse;
/**
 * Returns the string representation for the UUID's given bytes.
 */
function unparse(bytes) {
    // UUID format: [3][2][1][0]-[5][4]-[7][6]-[8][9]-[10][11][12][13][14][15]
    if (bytes instanceof Buffer) {
        bytes = Array.prototype.slice.call(bytes);
    }
    return [
        bytes.slice(0, 4).reverse(),
        bytes.slice(4, 6).reverse(),
        bytes.slice(6, 8).reverse(),
        bytes.slice(8, 10),
        bytes.slice(10, 16)
    ]
        .map(section => section
        .map(byte => {
        const s = byte.toString(16);
        return byte > 15 ? s : "0" + s;
    })
        .join(""))
        .join("-");
}
exports.unparse = unparse;
